﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VilaReal
{
    /// <summary>
    /// Lógica interna para FrmCadEmpresa.xaml
    /// </summary>
    public partial class FrmCadEmpresa : Window
    {
        public FrmCadEmpresa()
        {
            InitializeComponent();
            BtnNovo.Focus();
        }
        private void BtnNovo_Click(object sender, RoutedEventArgs e)
        {
            TxtNome.IsEnabled = true;
            TxtNome.Focus();
            BtnLimpar.IsEnabled = true;
            BtnNovo.IsEnabled = false;
        }

        private void TxtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxtEmail.IsEnabled = true;
                TxtEmail.Focus();
            }
        }

        private void TxtEmail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxtSenha.IsEnabled = true;
                TxtSenha.Focus();
            }
        }

        private void TxtSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxtDataCadastro.Text = DateTime.Now.ToShortDateString();
                TxtEndereco.IsEnabled = true;
                TxtEndereco.Focus();
            }
        }

        private void TxtEndereco_KeyDown(object sender, KeyEventArgs e)
        {
            TxtNumero.IsEnabled = true;
            TxtNumero.Focus();
        }

        private void TxtNumero_KeyDown(object sender, KeyEventArgs e)
        {
            TxtComplemento.IsEnabled = true;
            TxtComplemento.Focus();
        }

        private void TxtComplemento_KeyDown(object sender, KeyEventArgs e)
        {
            TxtBairro.IsEnabled = true;
            TxtBairro.Focus();
        }

        private void TxtBairro_KeyDown(object sender, KeyEventArgs e)
        {
            TxtCidade.IsEnabled = true;
            TxtCidade.Focus();
        }

        private void TxtCidade_KeyDown(object sender, KeyEventArgs e)
        {
            TxtUf.IsEnabled = true;
            TxtUf.Focus();
        }

        private void TxtUf_KeyDown(object sender, KeyEventArgs e)
        {
            {
                TxtCep.IsEnabled = true;
                TxtCep.Focus();
            }
        }

        private void TxtCep_KeyDown(object sender, KeyEventArgs e)
        {
            {
                TxtSite.IsEnabled = true;
                TxtSite.Focus();
            }
        }

        private void BtnLimpar_Click(object sender, RoutedEventArgs e)
        {
            TxtNome.Clear();
            TxtEmail.Clear();
            TxtSenha.Clear();
            TxtDataCadastro.Clear();
            TxtEndereco.Clear();
            TxtNumero.Clear();
            TxtComplemento.Clear();
            TxtBairro.Clear();
            TxtCidade.Clear();
            TxtUf.Clear();
            TxtCep.Clear();
            TxtSite.Clear();

            TxtNome.IsEnabled = false;
            TxtEmail.IsEnabled = false;
            TxtSenha.IsEnabled = false;
            TxtEndereco.IsEnabled = false;
            TxtNumero.IsEnabled = false;
            TxtComplemento.IsEnabled = false;
            TxtBairro.IsEnabled = false;
            TxtCidade.IsEnabled = false;
            TxtUf.IsEnabled = false;
            TxtCep.IsEnabled = false;
            TxtSite.IsEnabled = false;
            BtnLimpar.IsEnabled = false;
            BtnSalvar.IsEnabled = false;
            BtnNovo.IsEnabled = true;
            BtnNovo.Focus();
        }

        private void BtnSair_Click(object sender, RoutedEventArgs e)
        {
            MainWindow frm = new MainWindow();
            frm.Show();

            this.Close();
        }

        private void BtnSalvar_Click(object sender, RoutedEventArgs e)
        {
            DateTime dataCadastro = DateTime.Today;

            Banco bd = new Banco();
            bd.Conectar();
            if (ChkStatus.IsChecked == true)
            {
                status = "ATIVO";
                String inserir = "INSERT INTO funcionario(nomeFunc,emailFunc,senhaFunc,nivelFunc,statusFunc,dataCadFunc,idEmpresa)VALUES('" +
                    TxtNome.Text + "','" +
                    TxtEmail.Text + "','" +
                    TxtSenha.Text + "','" +
                    CmbNivel.Text + "','" +
                    status + "','" +
                    dataCadastro.ToString("yyyy-MM-dd") + "','" +
                    codigoEmp + "')";
                MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
                comandos.ExecuteNonQuery();
            }
            else
            {
                status = "INATIVO";
                string inserir = "INSERT INTO funconario(nomeFunc,emailFunc,senhaFunc,nivelFunc,statusFunc,dataCadFunc,idEmpresa)VALUES('" +
                    TxtNome.Text + "','" +
                    TxtEmail.Text + "','" +
                    TxtSenha.Text + "','" +
                    CmbNivel.Text + "','" +
                    status + "','" +
                    dataCadastro.ToString("yyyy-MM-dd") + "','" +
                    codigoEmp + "')";

                MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
                comandos.ExecuteNonQuery();
            }

            bd.Desconectar();
            MessageBox.Show("Funcionário cadastrado com sucesso!!!", "Cadastro de Funcionários");
            BtnLimpar.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));

            var tel = MessageBox.Show(
                "Deseja inserir um número de telefone para este funcionário?",
                "Cadadtro Telefone",
                MessageBoxButton.YesNo,
                MessageBoxImage.Exclamation);

            if (tel != MessageBoxResult.Yes)
            {
                return;
            }
            else
            {
                FrmCadTelEmpresa frm = new FrmCadTelEmpresa();
                frm.Show();

                this.Close();
            }
        
        }
    }
}
