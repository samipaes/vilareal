﻿#pragma checksum "..\..\FrmCadTelCliente.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5D103F3169246335E248A867F9C8AAF31A83347F"
//------------------------------------------------------------------------------
// <auto-generated>
//     O código foi gerado por uma ferramenta.
//     Versão de Tempo de Execução:4.0.30319.42000
//
//     As alterações ao arquivo poderão causar comportamento incorreto e serão perdidas se
//     o código for gerado novamente.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using ViralReal;


namespace ViralReal {
    
    
    /// <summary>
    /// FrmCadTelCliente
    /// </summary>
    public partial class FrmCadTelCliente : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 1 "..\..\FrmCadTelCliente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal ViralReal.FrmCadTelCliente FrmCadTelCliente1;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\FrmCadTelCliente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblCadTelCliente;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\FrmCadTelCliente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblNome;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\FrmCadTelCliente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox CmbNome;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\FrmCadTelCliente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblTelefone;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\FrmCadTelCliente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtTelefone;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\FrmCadTelCliente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblTipo;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\FrmCadTelCliente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox CmbTipo;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\FrmCadTelCliente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LblDescricao;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\FrmCadTelCliente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox CmbDescricao;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\FrmCadTelCliente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnSalvar;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\FrmCadTelCliente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnLimpar;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\FrmCadTelCliente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnSair;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/VilaReal;component/frmcadtelcliente.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\FrmCadTelCliente.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.FrmCadTelCliente1 = ((ViralReal.FrmCadTelCliente)(target));
            return;
            case 2:
            this.LblCadTelCliente = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.LblNome = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.CmbNome = ((System.Windows.Controls.ComboBox)(target));
            
            #line 18 "..\..\FrmCadTelCliente.xaml"
            this.CmbNome.KeyDown += new System.Windows.Input.KeyEventHandler(this.CmbNome_KeyDown);
            
            #line default
            #line hidden
            return;
            case 5:
            this.LblTelefone = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.TxtTelefone = ((System.Windows.Controls.TextBox)(target));
            
            #line 20 "..\..\FrmCadTelCliente.xaml"
            this.TxtTelefone.KeyDown += new System.Windows.Input.KeyEventHandler(this.TxtTelefone_KeyDown);
            
            #line default
            #line hidden
            return;
            case 7:
            this.LblTipo = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.CmbTipo = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 9:
            this.LblDescricao = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.CmbDescricao = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 11:
            this.BtnSalvar = ((System.Windows.Controls.Button)(target));
            
            #line 25 "..\..\FrmCadTelCliente.xaml"
            this.BtnSalvar.Click += new System.Windows.RoutedEventHandler(this.BtnSalvar_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.BtnLimpar = ((System.Windows.Controls.Button)(target));
            
            #line 33 "..\..\FrmCadTelCliente.xaml"
            this.BtnLimpar.Click += new System.Windows.RoutedEventHandler(this.BtnSalvar_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.BtnSair = ((System.Windows.Controls.Button)(target));
            
            #line 34 "..\..\FrmCadTelCliente.xaml"
            this.BtnSair.Click += new System.Windows.RoutedEventHandler(this.BtnSalvar_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

